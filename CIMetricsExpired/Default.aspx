﻿<%@ Page Title="Link Expired" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CIMetricsExpired._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">


<div>

<p><b><o:p>&nbsp;</o:p></b></p>

<p><b><o:p>&nbsp;</o:p></b></p>

<p style='text-indent:.5in'><b>The St. David’s Foundation Metrics Application has moved. <o:p></o:p></b></p>

<p style='text-indent:.5in'>Please check your grant portal email for an updated link to your metrics web page.<o:p></o:p></p> 

<p style='text-indent:.5in'>The link was included in the reporting requirements email sent to all grant partners. <o:p></o:p></p>

<p><o:p>&nbsp;</o:p></p>

<p style='text-indent:.5in'>For assistance please email <a href="mailto:grants@stdavidsfoundation.org">grants@stdavidsfoundation.org</a>.</p>

</div>

</asp:Content>
